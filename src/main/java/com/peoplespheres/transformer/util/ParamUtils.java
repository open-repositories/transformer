package com.peoplespheres.transformer.util;

import com.peoplespheres.transformer.exception.ParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

import static com.peoplespheres.transformer.util.ExceptionUtils.logAndRaise;

public class ParamUtils {

	private static final Logger logger = LoggerFactory.getLogger(ParamUtils.class);

	private ParamUtils() {
	}

	public static String getParamValue(final String target, final Map<String, String> params) {
		return Optional.ofNullable(params.get(target))
				.filter(param -> !param.isEmpty())
				.orElseThrow(() ->
						logAndRaise(
								logger,
								String.format("Query parameters do not contain expression target '%s'", target),
								ParameterException.class
						)
				);
	}

}


