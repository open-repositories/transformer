package com.peoplespheres.transformer.util;

import com.peoplespheres.transformer.exception.ExceptionDetails;
import com.peoplespheres.transformer.exception.GenericException;
import com.peoplespheres.transformer.exception.TransformerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class ExceptionUtils {

	private static final Logger localLogger = LoggerFactory.getLogger(ExceptionUtils.class);

	private ExceptionUtils() {
	}

	public static <T extends TransformerException> T logAndRaise(
			final Logger logger,
			final String message,
			final Class<T> exceptionClass
	) {
		logger.error(message);
		final var exceptionDetails = new ExceptionDetails(BAD_REQUEST, message, null);
		try {
			return exceptionClass.getConstructor(ExceptionDetails.class).newInstance(exceptionDetails);
		} catch (Exception e) {
			final var errorMessage = "Error while throwing exception";
			localLogger.error(errorMessage);
			final var details = new ExceptionDetails(INTERNAL_SERVER_ERROR, errorMessage, e);
			throw new GenericException(details);
		}
	}
}
