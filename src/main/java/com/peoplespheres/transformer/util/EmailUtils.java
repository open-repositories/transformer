package com.peoplespheres.transformer.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;

import static com.peoplespheres.transformer.util.EncryptionUtils.obfuscate;

public class EmailUtils {

	private static final Logger logger = LoggerFactory.getLogger(EmailUtils.class);

	private EmailUtils() {
	}

	private static final HashSet<Character> nameAllowedChars = new HashSet<>();
	private static final HashSet<Character> domainAllowedChars = new HashSet<>();

	static {
		for (char c : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+_-~.".toCharArray()) {
			nameAllowedChars.add(c);
		}

		for (char c : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.".toCharArray()) {
			domainAllowedChars.add(c);
		}
	}

	public static String filterIllegalCharacters(final String email) {
		boolean isDomainPart = false;

		final var filtered = new StringBuilder();

		for (char character : email.toCharArray()) {
			if (character == '@') {
				isDomainPart = true;
				filtered.append(character);
				continue;
			}

			if (isValidCharacter(character, isDomainPart)) {
				filtered.append(character);
			}
		}

		final var filteredEmail = filtered.toString();

		if (!email.equals(filteredEmail) && logger.isInfoEnabled()) {
			logger.info("Filtered email with illegal characters: {} to {}", email, obfuscate(filteredEmail));
		}

		return filteredEmail;
	}

	private static boolean isValidCharacter(char character, boolean isDomain) {
		return isDomain ? domainAllowedChars.contains(character) : nameAllowedChars.contains(character);
	}

}
