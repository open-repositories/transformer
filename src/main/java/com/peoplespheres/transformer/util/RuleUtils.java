package com.peoplespheres.transformer.util;

import com.peoplespheres.transformer.exception.RuleException;
import com.peoplespheres.transformer.shared.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

import static com.peoplespheres.transformer.util.ExceptionUtils.logAndRaise;
import static java.util.regex.Pattern.DOTALL;

public class RuleUtils {

	private static final Logger logger = LoggerFactory.getLogger(RuleUtils.class);

	private RuleUtils() {
	}

	private static final String RULE_ARGUMENT_PATTERN = "\\(([^)]+)\\)";
	private static final String DIGITS_PATTERN = "\\d+";

	public static Rule getRule(String rule) {
		final var ruleName = rule.contains("(") ? rule.substring(0, rule.indexOf("(")) : rule;

		return Rule.fromName(ruleName).orElseThrow(() ->
				logAndRaise(
						logger,
						String.format("Unknown rule '%s'", ruleName),
						RuleException.class
				)
		);

	}

	public static Integer extractIntegerArgument(String rule) {
		final var pattern = Pattern.compile(RULE_ARGUMENT_PATTERN, DOTALL);
		final var matcher = pattern.matcher(rule);
		if (matcher.find()) {
			final var match = matcher.group(1);
			if (match.matches(DIGITS_PATTERN)) {
				return Integer.parseInt(match);
			} else {
				throw logAndRaise(
						logger,
						String.format("Content '%s' between parentheses of rule '%s' is not a valid Integer argument", match, rule),
						RuleException.class
				);
			}
		} else {
			throw logAndRaise(
					logger,
					String.format("Rule '%s' uses illegal parentheses. Allowed parentheses are '()'", rule),
					RuleException.class
			);
		}
	}
}
