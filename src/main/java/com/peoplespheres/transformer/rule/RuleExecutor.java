package com.peoplespheres.transformer.rule;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.peoplespheres.transformer.rule.StringOperation.FROM_END;
import static com.peoplespheres.transformer.rule.StringOperation.FROM_START;
import static com.peoplespheres.transformer.util.RuleUtils.extractIntegerArgument;
import static com.peoplespheres.transformer.util.RuleUtils.getRule;

public class RuleExecutor {

	private static final Logger logger = LoggerFactory.getLogger(RuleExecutor.class);

	private RuleExecutor() {
	}

	public static String executeRule(final String ruleLiteral, final String value) {
		final var rule = getRule(ruleLiteral);

		return switch (rule) {
			case USE_FIRST -> processValue(FROM_START, ruleLiteral, value);
			case USE_LAST -> processValue(FROM_END, ruleLiteral, value);
			default -> value;
		};

	}

	private static String processValue(final StringOperation operation, final String ruleLiteral, final String value) {
		final int argument = extractIntegerArgument(ruleLiteral);

		if (argument >= value.length()) {
			logger.warn("Rule length is greater than the value's length. Returning full value.");
			return value;
		} else if (operation == FROM_START) {
			return value.substring(0, argument);
		} else {
			final var startIndex = value.length() - argument;
			return value.substring(startIndex);
		}
	}
}

