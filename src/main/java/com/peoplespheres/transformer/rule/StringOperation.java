package com.peoplespheres.transformer.rule;

public enum StringOperation {
	FROM_START,
	FROM_END
}
