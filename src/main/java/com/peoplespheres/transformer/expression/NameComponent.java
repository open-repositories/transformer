package com.peoplespheres.transformer.expression;

import java.util.List;

public record NameComponent(List<ComponentElement> elements) {
}
