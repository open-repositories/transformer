package com.peoplespheres.transformer.expression;

import com.peoplespheres.transformer.exception.ExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static com.peoplespheres.transformer.util.ExceptionUtils.logAndRaise;
import static java.util.regex.Pattern.DOTALL;

public class EmailExpressionBuilder {

	private static final Logger logger = LoggerFactory.getLogger(EmailExpressionBuilder.class);

	private static final String EXPRESSION_COMPONENTS_PATTERN = "(?<=\\[).+(?=])";
	private static final String COMPONENT_ELEMENTS_PATTERN = "\\{(.+?)}";
	private static final String ELEMENT_SEPARATOR_PATTERN = "\\s*:\\s*";

	private EmailExpressionBuilder() {
	}

	public static EmailExpression buildExpression(final String input) {
		final var components = getComponents(input);
		final var name = new NameComponent(getElements(components[0]));
		final var domain = new DomainComponent(getElements(components[1]));
		return new EmailExpression(name, domain);
	}

	private static String[] getComponents(final String input) {
		final var pattern = Pattern.compile(EXPRESSION_COMPONENTS_PATTERN, DOTALL);
		final var matcher = pattern.matcher(input);

		if (matcher.find()) {
			final var result = matcher.group();
			return result.split(";");
		} else {
			throw logAndRaise(
					logger,
					"Illegal expression outer parenthesis. Valid outer parenthesis are: '[]'",
					ExpressionException.class
			);
		}
	}

	private static List<ComponentElement> getElements(final String input) {
		final var pattern = Pattern.compile(COMPONENT_ELEMENTS_PATTERN, DOTALL);
		final var matcher = pattern.matcher(input);
		if (matcher.find()) {
			final var result = matcher.group();
			final var splitResult = result.replaceAll("[{}]", "").trim().split("\\s*,\\s*");

			return Arrays.stream(splitResult)
					.map(element -> extractValues(element.trim()))
					.filter(Optional::isPresent)
					.map(Optional::get)
					.map(values -> new ComponentElement(values[0], values[1]))
					.toList();
		} else {
			throw logAndRaise(
					logger,
					"Illegal component parenthesis. Valid component parenthesis are: '{}'",
					ExpressionException.class
			);
		}
	}

	private static Optional<String[]> extractValues(String input) {
		final var values = input.split(ELEMENT_SEPARATOR_PATTERN);

		if (values.length == 2) {
			return Optional.of(values);
		} else {
			throw logAndRaise(
					logger,
					String.format("Error: Illegal expression component format: '%s'. Valid format is 'key: value'.", input),
					ExpressionException.class
			);
		}
	}
}