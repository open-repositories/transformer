package com.peoplespheres.transformer.expression;

public record EmailExpression(NameComponent name, DomainComponent domain) {
}

