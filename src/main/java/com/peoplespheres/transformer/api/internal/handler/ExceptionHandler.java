package com.peoplespheres.transformer.api.internal.handler;

import com.peoplespheres.transformer.exception.TransformerException;
import com.peoplespheres.transformer.model.ErrorResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Component
@RequiredArgsConstructor
public class ExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

	public Mono<ServerResponse> handleException(final Throwable throwable) {
		if (throwable instanceof TransformerException transformerException) {
			final var httpStatus = transformerException.getDetails().serviceStatus();
			final var message = transformerException.getDetails().message();
			final var errorResponse = new ErrorResponse(httpStatus, httpStatus.value(), message, LocalDateTime.now());

			logger.info("Handling '{}' with status: '{}'. Error: '{}'",
					transformerException.getClass().getSimpleName(), httpStatus.value(), message);

			return ServerResponse.status(httpStatus).contentType(APPLICATION_JSON).bodyValue(errorResponse);
		} else {
			final var httpStatus = INTERNAL_SERVER_ERROR;
			final var errorResponse = new ErrorResponse(httpStatus, httpStatus.value(), throwable.getMessage(), LocalDateTime.now());

			logger.info("Handling unexpected error: '{}'", throwable.getMessage());

			return ServerResponse.status(httpStatus).contentType(APPLICATION_JSON).bodyValue(errorResponse);
		}
	}
}
