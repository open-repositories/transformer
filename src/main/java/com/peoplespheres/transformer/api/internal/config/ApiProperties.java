package com.peoplespheres.transformer.api.internal.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "service.api")
@Getter
@Setter
public class ApiProperties {
	private String version;
	private UriProperties uri;

	@Getter
	@Setter
	public static class UriProperties {
		private GenerateUriProperties generate;
	}

	@Getter
	@Setter
	public static class GenerateUriProperties {
		private String base;
		private String email;
	}

}


