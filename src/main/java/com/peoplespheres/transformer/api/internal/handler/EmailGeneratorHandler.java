package com.peoplespheres.transformer.api.internal.handler;


import com.peoplespheres.transformer.generator.external.api.GenerateEmails;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static com.peoplespheres.transformer.api.internal.util.RequestUtils.getExpression;
import static com.peoplespheres.transformer.api.internal.util.RequestUtils.getParams;
import static com.peoplespheres.transformer.expression.EmailExpressionBuilder.buildExpression;
import static com.peoplespheres.transformer.util.EncryptionUtils.decodeFromBase64;

@Component
@RequiredArgsConstructor
public class EmailGeneratorHandler {

	private final GenerateEmails generateEmail;
	private final ExceptionHandler exceptionHandler;

	public Mono<ServerResponse> generateEmail(final ServerRequest request) {
		return Mono.defer(() -> {
							final var params = getParams(request);
							final var expression = buildExpression(decodeFromBase64(getExpression(request)));
							final var product = generateEmail.generate(expression, params);
							return Mono.just(product);
						}
				)
				.flatMap(result -> ServerResponse.ok().bodyValue(result))
				.onErrorResume(exceptionHandler::handleException);
	}
}