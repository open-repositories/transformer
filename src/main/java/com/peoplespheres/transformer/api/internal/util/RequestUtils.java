package com.peoplespheres.transformer.api.internal.util;

import com.peoplespheres.transformer.exception.ParameterException;
import com.peoplespheres.transformer.shared.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.LinkedList;

import static com.peoplespheres.transformer.util.ExceptionUtils.logAndRaise;

public class RequestUtils {

	private static final Logger logger = LoggerFactory.getLogger(RequestUtils.class);
	private static final String EXPRESSION = Param.EXPRESSION.getName();

	private RequestUtils() {
	}

	public static String getExpression(final ServerRequest request) {
		return request
				.queryParam(EXPRESSION)
				.map(expression -> {
					if (expression.isEmpty()) {
						throw logAndRaise(
								logger,
								"Expression parameter is empty",
								ParameterException.class
						);
					}
					return expression;
				})
				.orElseThrow(() ->
						logAndRaise(
								logger,
								"Expression parameter is missing",
								ParameterException.class
						));
	}

	public static LinkedMultiValueMap<String, String> getParams(final ServerRequest request) {
		final var params = new LinkedMultiValueMap<String, String>();

		request.queryParams().forEach((key, value) -> {
					if (!key.equals(EXPRESSION)) {
						final var param = Param.fromName(key).orElseThrow(() ->
								logAndRaise(logger,
										String.format("Invalid query parameter '%s'", key),
										ParameterException.class
								)
						);
						params.put(param.getName(), new LinkedList<>(value));
					}
				}
		);
		return params;
	}
}
