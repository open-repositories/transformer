package com.peoplespheres.transformer.exception;

public final class RuleException extends TransformerException {

	public RuleException(ExceptionDetails details) {
		super(details);
	}
}
