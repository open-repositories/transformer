package com.peoplespheres.transformer.exception;

import org.springframework.http.HttpStatus;

public record ExceptionDetails(
		HttpStatus serviceStatus,
		String message,
		Throwable cause
) {
}
