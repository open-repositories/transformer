package com.peoplespheres.transformer.exception;

import lombok.Getter;

@Getter
public abstract sealed class TransformerException extends RuntimeException permits
		GenericException,
		ExpressionException,
		ParameterException,
		RuleException,
		ValidationException {

	private final ExceptionDetails details;

	protected TransformerException(ExceptionDetails details) {
		super(details.message(), details.cause());
		this.details = details;
	}
}

