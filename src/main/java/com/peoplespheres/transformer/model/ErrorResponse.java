package com.peoplespheres.transformer.model;

import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Schema(description = "Error container")
public record ErrorResponse(
		@Schema(description = "String representation of the status code")
		HttpStatus httpStatus,

		@Schema(description = "Integer representation of the status code")
		Integer serviceStatus,

		@Schema(description = "Error message detailing the nature of the error")
		String message,

		@Schema(description = "Timestamp of when the error occurred")
		LocalDateTime timestamp
) {
}
