package com.peoplespheres.transformer.integration;

import com.peoplespheres.transformer.api.internal.config.ApiProperties;
import com.peoplespheres.transformer.model.Emails;
import com.peoplespheres.transformer.model.ErrorResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("local")
class GenerateEmailHandlerTest {

	@Autowired private WebTestClient webTestClient;
	@Autowired private ApiProperties api;

	@Test
	@DisplayName("Given valid arguments returns expected output")
	void givenValidArgumentsReturnsExpectedOutput() {
		final var encodedExpression = "Z2VuZXJhdGVFbWFpbFsNCgl3aXRoTmFtZUFzew0KCQlmaXJzdE5hbWU6IHVzZUFsbCwNCgkJbGFzdE5hbWU6IHVzZUFsbCwNCgkJY29uY2F0ZW5hdGVXaXRoOiBfDQoJfTsNCglhdERvbWFpbkFzew0KCQllbnRpdHk6IHVzZUFsbCwNCgkJZG9tYWluOiB1c2VBbGwNCgl9DQpd";
		final var expected = "ivo_georgiev@peoplespheres.com";
		final var base = api.getUri().getGenerate().getBase();
		final var email = api.getUri().getGenerate().getEmail();
		final var uri = String.format("%s%s?firstName=ivo&lastName=georgiev&entity=peoplespheres&domain=com&expression=%s", base, email, encodedExpression);

		webTestClient.get().uri(uri)
				.accept(APPLICATION_JSON)
				.exchange()
				.expectStatus().isOk()
				.expectBody(Emails.class)
				.consumeWith(response -> {
					assertNotNull(response.getResponseBody());
					final var data = response.getResponseBody().data();
					assertEquals(data.getFirst().id(), expected);
					assertEquals(data.getFirst().value(), expected);
				});
	}

	@Test
	@DisplayName("Given missing request expression parameter returns ErrorResponse")
	void givenMissingRequestExpressionParameterReturnsErrorResponse() {
		final var base = api.getUri().getGenerate().getBase();
		final var email = api.getUri().getGenerate().getEmail();
		final var InvalidUriWithMissingExpression = String.format("%s%s?firstName=ivo&lastName=georgiev&entity=peoplespheres&domain=com", base, email);
		webTestClient.get().uri(InvalidUriWithMissingExpression)
				.accept(APPLICATION_JSON)
				.exchange()
				.expectStatus().is4xxClientError()
				.expectBody(ErrorResponse.class)
				.consumeWith(response -> {
					assertNotNull(response.getResponseBody());
					assertEquals(BAD_REQUEST.value(), response.getResponseBody().serviceStatus());
				});
	}

	@Test
	@DisplayName("Given empty request expression parameter returns ErrorResponse")
	void givenEmptyRequestExpressionParameterReturnsErrorResponse() {
		final var base = api.getUri().getGenerate().getBase();
		final var email = api.getUri().getGenerate().getEmail();
		final var InvalidUriWithEmptyExpression = String.format("%s%s?firstName=ivo&lastName=georgiev&entity=peoplespheres&domain=com&expression=", base, email);
		webTestClient.get().uri(InvalidUriWithEmptyExpression)
				.accept(APPLICATION_JSON)
				.exchange()
				.expectStatus().is4xxClientError()
				.expectBody(ErrorResponse.class)
				.consumeWith(response -> {
					assertNotNull(response.getResponseBody());
					assertEquals(BAD_REQUEST.value(), response.getResponseBody().serviceStatus());
				});
	}

	@Test
	@DisplayName("Given invalid query param returns ErrorResponse")
	void givenInvalidQueryParamReturnsErrorResponse() {
		final var base = api.getUri().getGenerate().getBase();
		final var email = api.getUri().getGenerate().getEmail();
		final var invalidParam = "invalidParam";
		final var InvalidUriWithMissingExpression = String.format("%s%s?%s=ivo&lastName=georgiev&entity=peoplespheres&domain=com&expression=some-expression", base, email, invalidParam);
		webTestClient.get().uri(InvalidUriWithMissingExpression)
				.accept(APPLICATION_JSON)
				.exchange()
				.expectStatus().is4xxClientError()
				.expectBody(ErrorResponse.class)
				.consumeWith(response -> {
					assertNotNull(response.getResponseBody());
					assertEquals(BAD_REQUEST.value(), response.getResponseBody().serviceStatus());
				});
	}
}
