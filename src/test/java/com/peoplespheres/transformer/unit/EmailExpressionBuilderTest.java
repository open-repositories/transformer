package com.peoplespheres.transformer.unit;

import com.peoplespheres.transformer.exception.ExpressionException;
import com.peoplespheres.transformer.expression.ComponentElement;
import com.peoplespheres.transformer.expression.DomainComponent;
import com.peoplespheres.transformer.expression.EmailExpression;
import com.peoplespheres.transformer.expression.NameComponent;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static com.peoplespheres.transformer.expression.EmailExpressionBuilder.buildExpression;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class EmailExpressionBuilderTest {

	@ParameterizedTest
	@MethodSource("provideValidExpressions")
	@DisplayName("Given valid inputs successfully returns EmailExpression object")
	void givenValidInputsSuccessfullyReturnsExpression(String validExpression) {
		final var expected = new EmailExpression(
				new NameComponent(List.of(
						new ComponentElement("firstName", "useAll"),
						new ComponentElement("lastName", "useAll"),
						new ComponentElement("concatenateWith", "_")
				)
				),
				new DomainComponent(List.of(
						new ComponentElement("entity", "useAll"),
						new ComponentElement("domain", "useAll")
				)
				)
		);

		final var actual = buildExpression(validExpression);

		assertEquals(expected, actual);
	}

	@Test
	@DisplayName("Given invalid input with bad outer bracket throws exception")
	void givenInvalidOuterBracketsThrowsException() {
		final var invalidOuterBracket = "{";
		final var invalidInput = String.format("""
				generateEmail%s
				\twithNameAs{
				\t\tfirstName: useAll,
				\t\tlastName: useAll,
				\t\tconcatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""", invalidOuterBracket);

		assertThatThrownBy(() -> buildExpression(invalidInput)).isInstanceOf(ExpressionException.class);
	}

	@Test
	@DisplayName("Given invalid input with bad inner bracket throws exception")
	void givenInvalidInnerBracketsThrowsException() {
		final var invalidInnerBracket = "[";
		final var invalidInput = String.format("""
				generateEmail[
				\twithNameAs%s
				\t\tfirstName: useAll,
				\t\tlastName: useAll,
				\t\tconcatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""", invalidInnerBracket);

		assertThatThrownBy(() -> buildExpression(invalidInput)).isInstanceOf(ExpressionException.class);
	}

	@Test
	@DisplayName("Given invalid input with missing ':' throws exception")
	void givenMissingColonElementSeparatorThrowsException() {
		final var invalidComponentWithMissingColon = "firstName useAll";
		final var invalidInput = String.format("""
				generateEmail[
				\twithNameAs{
				\t\t%s,
				\t\tlastName: useAll,
				\t\tconcatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""", invalidComponentWithMissingColon);

		assertThatThrownBy(() -> buildExpression(invalidInput)).isInstanceOf(ExpressionException.class);
	}

	private static Stream<String> provideValidExpressions() {
		final var validConsistentInput = """
				generateEmail[
				\twithNameAs{
				\t\tfirstName: useAll,
				\t\tlastName: useAll,
				\t\tconcatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""";

		final var validInputWithNonConsistentSpacing = """
				generateEmail[
				\twithNameAs{
				\tfirstName: useAll,
				\t\tlastName: useAll,
				concatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\tdomain: useAll
				}
				]""";

		final var validInputWithNonConsistentElementSeparation = """
				generateEmail[
				\twithNameAs{
				\tfirstName:useAll,
				\t\tlastName  : useAll,
				concatenateWith:   _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\tdomain: useAll
				}
				]""";

		final var validInputWithNewLines = """
				generateEmail[
								
				\twithNameAs{
								
				\tfirstName: useAll,
				\t\tlastName: useAll,
				\n\t
				concatenateWith: _
				\t};
				\t
				atDomainAs{
				\t\tentity: useAll,
				\tdomain: useAll
				}
				]""";

		final var validInputWithNoSpaces = "generateEmail[withNameAs{firstName:useAll,lastName:useAll,concatenateWith:_};atDomainAs{entity:useAll,domain:useAll}]";

		return Stream.of(
				validConsistentInput,
				validInputWithNonConsistentSpacing,
				validInputWithNonConsistentElementSeparation,
				validInputWithNewLines,
				validInputWithNoSpaces
		);
	}

}
