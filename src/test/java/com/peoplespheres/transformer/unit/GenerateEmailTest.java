package com.peoplespheres.transformer.unit;


import com.peoplespheres.transformer.exception.ExpressionException;
import com.peoplespheres.transformer.exception.ParameterException;
import com.peoplespheres.transformer.exception.RuleException;
import com.peoplespheres.transformer.generator.external.api.GenerateEmails;
import com.peoplespheres.transformer.generator.internal.email.usecase.generate.GenerateEmailsImpl;
import com.peoplespheres.transformer.generator.internal.validation.TransformerValidator;
import com.peoplespheres.transformer.model.Email;
import com.peoplespheres.transformer.model.Emails;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;
import java.util.stream.Stream;

import static com.peoplespheres.transformer.expression.EmailExpressionBuilder.buildExpression;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GenerateEmailTest {

	private final GenerateEmails subject = new GenerateEmailsImpl(new TransformerValidator());

	@ParameterizedTest(name = "[{index}] Successfully generates email: {1}")
	@MethodSource("generateValidExpressionAndExpectedOutput")
	void givenValidArgumentsSuccessfullyGeneratesEmailWithFullName(final String expression, final String expectedEmail) {
		final var parsedExpression = buildExpression(expression);
		final var validLowercaseParams = getValidParamsAllLowercase();
		final var validMixedCaseParams = getValidParamsMixedCase();

		assertEquals(expectedEmail, subject.generate(parsedExpression, validLowercaseParams).data().getFirst().id());
		assertEquals(expectedEmail, subject.generate(parsedExpression, validMixedCaseParams).data().getFirst().id());
	}

	@Test
	@DisplayName("Given name rule argument bigger than name length falls back to full name")
	void givenNameRuleArgumentBiggerThanNameFallsBackToFullName() {
		final var expression = """
				generateEmail[
				\twithNameAs{
				\t\tfirstName: useFirst(5),
				\t\tlastName: useLast(10),
				\t\tconcatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""";
		final var parsedExpression = buildExpression(expression);
		final var params = getValidParamsAllLowercase();
		final var expectedEmail = "ivo_georgiev@peoplespheres.com";

		assertEquals(expectedEmail, subject.generate(parsedExpression, params).data().getFirst().id());
	}


	@Test
	@DisplayName("Given query params for 3 users and shared domain values successfully generates 3 emails")
	void givenQueryParamsFor3UsersAndSharedDomainValuesSuccessfullyGenerates3Emails() {
		final var expression = """
				generateEmail[
				\twithNameAs{
				\t\tfirstName: useAll,
				\t\tmiddleName: useAll,
				\t\tlastName: useAll,
				\t\tconcatenateWith: .
				\t};
				\tatDomainAs{
				\t\tresourceType: useAll,
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""";

		final var parsedExpression = buildExpression(expression);

		final var params = new LinkedMultiValueMap<String, String>();

		params.add("fullName", "Ivo Georgiev Georgiev");
		params.add("firstName", "ivo");
		params.add("firstName", "ventsislav");
		params.add("firstName", "venelina");
		params.add("middleName", "georgiev");
		params.add("middleName", "nikolaev");
		params.add("middleName", "ivanova");
		params.add("lastName", "todorov");
		params.add("lastName", "stoevski");
		params.add("lastName", "todorova");
		params.add("entity", "peoplespheres");
		params.add("domain", "com");
		params.add("resourceType", "external");
		params.add("entityDomain", "peoplespheres.com");
		params.add("role", "dev");
		params.add("department", "backend");

		final var result = subject.generate(parsedExpression, params);
		final var expected = new Emails(List.of(
				new Email("ivo.georgiev.todorov@external.peoplespheres.com",
						"ivo.georgiev.todorov@external.peoplespheres.com"
				),
				new Email("ventsislav.nikolaev.stoevski@external.peoplespheres.com",
						"ventsislav.nikolaev.stoevski@external.peoplespheres.com"
				),
				new Email("venelina.ivanova.todorova@external.peoplespheres.com",
						"venelina.ivanova.todorova@external.peoplespheres.com"
				)

		));

		assertEquals(result, expected);
	}

	@Test
	@DisplayName("Given query params for 2 users and unique domain values successfully generates 3 emails")
	void givenQueryParamsFor2UsersAndUniqueDomainValuesSuccessfullyGenerates2Emails() {
		final var expression = """
				generateEmail[
				\twithNameAs{
				\t\tfirstName: useAll,
				\t\tmiddleName: useAll,
				\t\tlastName: useAll,
				\t\tconcatenateWith: .
				\t};
				\tatDomainAs{
				\t\tresourceType: useAll,
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""";

		final var parsedExpression = buildExpression(expression);

		final var params = new LinkedMultiValueMap<String, String>();

		params.add("fullName", "Ivo Georgiev Georgiev");
		params.add("firstName", "ivo");
		params.add("middleName", "georgiev");
		params.add("lastName", "todorov");
		params.add("middleName", "nikolaev");
		params.add("domain", "com");
		params.add("domain", "tech");
		params.add("lastName", "stoevski");
		params.add("entity", "peoplespheres");
		params.add("resourceType", "external");
		params.add("firstName", "ventsislav");
		params.add("entity", "cordona");
		params.add("resourceType", "internal");
		params.add("role", "dev");
		params.add("department", "backend");
		params.add("entityDomain", "peoplespheres.com");

		final var result = subject.generate(parsedExpression, params);
		final var expected = new Emails(List.of(
				new Email("ivo.georgiev.todorov@external.peoplespheres.com",
						"ivo.georgiev.todorov@external.peoplespheres.com"
				),
				new Email("ventsislav.nikolaev.stoevski@internal.cordona.tech",
						"ventsislav.nikolaev.stoevski@internal.cordona.tech"
				)
		));

		assertEquals(result, expected);
	}

	@Test
	@DisplayName("Given expression with missing concatenation rule will throw an exception")
	void givenExpressionWithMissingConcatenationRuleThrowsException() {
		final var invalidExpressionWithMissingConcatenationRule = """
				generateEmail[
				\twithNameAs{
				\t\tfirstName: useAll,
				\t\tlastName: useAll,
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""";

		final var parsedExpression = buildExpression(invalidExpressionWithMissingConcatenationRule);
		final var validParams = getValidParamsAllLowercase();

		assertThatThrownBy(() -> subject.generate(parsedExpression, validParams)).isInstanceOf(ExpressionException.class);
	}

	@Test
	@DisplayName("Given expression with invalid rule will throw an exception")
	void givenExpressionWithInvalidRuleThrowsException() {
		final var invalidRule = "invalidRule";
		final var invalidExpression = String.format("""
				generateEmail[
				\twithNameAs{
				\t\tfirstName: %s,
				\t\tlastName: useAll,
				\t\tconcatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""", invalidRule);

		final var parsedExpression = buildExpression(invalidExpression);
		final var validParams = getValidParamsAllLowercase();

		assertThatThrownBy(() -> subject.generate(parsedExpression, validParams)).isInstanceOf(RuleException.class);
	}

	@ParameterizedTest()
	@MethodSource("generateInvalidParameters")
	void givenInvalidParamsThrowsException(final LinkedMultiValueMap<String, String> invalidParams) {
		final var validExpression = """
				generateEmail[
				\twithNameAs{
				\t\tfirstName: useAll,
				\t\tlastName: useAll,
				\t\tconcatenateWith: _
				\t};
				\tatDomainAs{
				\t\tentity: useAll,
				\t\tdomain: useAll
				\t}
				]""";

		final var parsedExpression = buildExpression(validExpression);

		assertThatThrownBy(() -> subject.generate(parsedExpression, invalidParams)).isInstanceOf(ParameterException.class);
	}

	@ParameterizedTest()
	@MethodSource("generateExpressionsWithInvalidRuleParentheses")
	void givenExpressionWithInvalidRuleParenthesesThrowsException(final String invalidExpression) {
		final var parsedExpression = buildExpression(invalidExpression);
		final var validParams = getValidParamsAllLowercase();

		assertThatThrownBy(() -> subject.generate(parsedExpression, validParams)).isInstanceOf(RuleException.class);
	}


	@ParameterizedTest()
	@MethodSource("generateExpressionsWithInvalidConcatenationCharacter")
	void givenExpressionWithInvalidConcatenationCharacterThrowsException(final String invalidExpression) {
		final var parsedExpression = buildExpression(invalidExpression);
		final var validParams = getValidParamsAllLowercase();

		assertThatThrownBy(() -> subject.generate(parsedExpression, validParams)).isInstanceOf(ExpressionException.class);
	}

	@ParameterizedTest()
	@MethodSource("generateExpressionsWithInvalidRuleArgument")
	void givenExpressionWithInvalidRuleArgumentThrowsException(final String invalidExpression) {
		final var parsedExpression = buildExpression(invalidExpression);
		final var validParams = getValidParamsAllLowercase();

		assertThatThrownBy(() -> subject.generate(parsedExpression, validParams)).isInstanceOf(RuleException.class);
	}

	private LinkedMultiValueMap<String, String> getValidParamsAllLowercase() {
		final var params = new LinkedMultiValueMap<String, String>();
		params.add("fullName", "Ivo Georgiev Georgiev");
		params.add("firstName", "ivo");
		params.add("middleName", "georgiev");
		params.add("lastName", "georgiev");
		params.add("entity", "peoplespheres");
		params.add("domain", "com");
		params.add("resourceType", "external");
		params.add("entityDomain", "peoplespheres.com");
		params.add("role", "dev");
		params.add("department", "backend");

		return params;
	}

	private LinkedMultiValueMap<String, String> getValidParamsMixedCase() {
		final var params = new LinkedMultiValueMap<String, String>();

		params.add("fullName", "Ivo Georgiev Georgiev");
		params.add("firstName", "Ivo");
		params.add("middleName", "georgiev");
		params.add("lastName", "georgiev");
		params.add("entity", "Peoplespheres");
		params.add("domain", "coM");
		params.add("resourceType", "EXTERNAL");
		params.add("entityDomain", "peoplespheres.Com");
		params.add("role", "dev");
		params.add("department", "backend");

		return params;
	}

	private static Stream<LinkedMultiValueMap<String, String>> generateInvalidParameters() {

		final var paramsWithMissingTarget = new LinkedMultiValueMap<String, String>();

		paramsWithMissingTarget.add("firstName", "ivo");
		paramsWithMissingTarget.add("lastName", "georgiev");
		paramsWithMissingTarget.add("domain", "com");
		paramsWithMissingTarget.add("resourceType", "external");
		paramsWithMissingTarget.add("entityDomain", "peoplespheres.com");

		final var paramsWithEmptyTarget = new LinkedMultiValueMap<String, String>();

		paramsWithEmptyTarget.add("firstName", "");
		paramsWithEmptyTarget.add("lastName", "georgiev");
		paramsWithEmptyTarget.add("entity", "peoplespheres");
		paramsWithEmptyTarget.add("domain", "com");
		paramsWithEmptyTarget.add("resourceType", "external");
		paramsWithEmptyTarget.add("entityDomain", "peoplespheres.com");

		return Stream.of(paramsWithMissingTarget, paramsWithEmptyTarget);
	}

	private static Stream<String> generateExpressionsWithInvalidConcatenationCharacter() {
		return Stream.of("""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst[1],
						\t\tlastName: useAll,
						\t\tconcatenateWith: *
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]""",
				"""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst[1],
						\t\tlastName: useAll,
						\t\tconcatenateWith: ""
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]""",
				"""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst[1],
						\t\tlastName: useAll,
						\t\tconcatenateWith: &
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]"""
		);
	}

	private static Stream<String> generateExpressionsWithInvalidRuleParentheses() {
		return Stream.of("""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst[1],
						\t\tlastName: useAll,
						\t\tconcatenateWith: _
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]""",
				"""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst/1/,
						\t\tlastName: useAll,
						\t\tconcatenateWith: _
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]""",
				"""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst(1],
						\t\tlastName: useAll,
						\t\tconcatenateWith: _
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]"""
		);
	}

	private static Stream<String> generateExpressionsWithInvalidRuleArgument() {
		return Stream.of("""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst(x),
						\t\tlastName: useAll,
						\t\tconcatenateWith: _
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]""",
				"""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst(1x),
						\t\tlastName: useAll,
						\t\tconcatenateWith: _
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]""",
				"""
						generateEmail[
						\twithNameAs{
						\t\tfirstName: useFirst(),
						\t\tlastName: useAll,
						\t\tconcatenateWith: _
						\t};
						\tatDomainAs{
						\t\tentity: useAll,
						\t\tdomain: useAll
						\t}
						]"""
		);
	}

	private static Stream<Arguments> generateValidExpressionAndExpectedOutput() {
		return Stream.of(
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useAll,
								\t\tlastName: useAll,
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"ivo_georgiev@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useFirst(1),
								\t\tlastName: useAll,
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"i_georgiev@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useFirst(2),
								\t\tlastName: useAll,
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"iv_georgiev@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useFirst(1),
								\t\tlastName: useFirst(1),
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"i_g@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useLast(1),
								\t\tlastName: useLast(1),
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"o_v@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useFirst(2),
								\t\tlastName: useLast(3),
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"iv_iev@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tlastName: useAll,
								\t\tconcatenateWith: .
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"georgiev@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useAll,
								\t\tconcatenateWith: .
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"ivo@peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useAll,
								\t\tlastName: useAll,
								\t\tconcatenateWith: .
								\t};
								\tatDomainAs{
								\t\tresourceType: useAll,
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"ivo.georgiev@external.peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useAll,
								\t\tlastName: useAll,
								\t\tconcatenateWith: .
								\t};
								\tatDomainAs{
								\t\tresourceType: useAll,
								\t\tentityDomain: useAll
								\t}
								]""",
						"ivo.georgiev@external.peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfullName: useAll,
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"ivo_georgiev_georgiev@peoplespheres.com"),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfullName: useFirst(3),
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"ivo_geo_geo@peoplespheres.com"),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useAll,
								\t\tmiddleName: useFirst(3),
								\t\tlastName: useAll,
								\t\tconcatenateWith: .
								\t};
								\tatDomainAs{
								\t\tresourceType: useAll,
								\t\tentityDomain: useAll
								\t}
								]""",
						"ivo.geo.georgiev@external.peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\trole: useAll,
								\t\tfirstName: useAll,
								\t\tmiddleName: useFirst(3),
								\t\tlastName: useAll,
								\t\tconcatenateWith: .
								\t};
								\tatDomainAs{
								\t\tresourceType: useAll,
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"dev.ivo.geo.georgiev@external.peoplespheres.com"
				),
				Arguments.of("""
								generateEmail[
								\twithNameAs{
								\t\tfirstName: useFirst(1),
								\t\tmiddleName: useFirst(1),
								\t\tlastName: useAll,
								\t\trole: useAll,
								\t\tdepartment: useAll,
								\t\tconcatenateWith: _
								\t};
								\tatDomainAs{
								\t\tresourceType: useAll,
								\t\tentity: useAll,
								\t\tdomain: useAll
								\t}
								]""",
						"i_g_georgiev_dev_backend@external.peoplespheres.com"
				)
		);

	}

}
