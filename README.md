# Rationale

The Email Transformer Service (ETS) is an application for transforming (generating) email(s) from http query parameters
based on rules described in a custom `PSDSEL` expression language.

# How to run the ETS Locally

1. Execute the following command from the root of the project:

```bash
docker-compose up
```

2. The `docker-compose` will orchestrate the ETS and a reverse-proxy service.
3. Do not access the ETS directly, only through the NGINX's exposed port `9443`
4. Use `https` for your calls.

# PSDSEL ([People Spheres](https://peoplespheres.com) Domain Specific Expression Language)

The ETS implements a custom `PSDSEL` specifically designed for the ETS service.

When designing the `PSDSEL`, we took into consideration the following business requirements:

## Syntax Business Requirements

1. Must improve the existing expression syntax;
2. Must be human-centric;
3. Must be easy to read and easy to understand;
4. Must be as close as possible to natural language paradigms;
5. It will be used primarily by developers or by professionals with coding and technical knowledge;
6. It will be developed as files containing one or more expressions;
7. The `PSDSEL` must align well with People Sphere’s strategic goal of developing internal tooling— IDE for
   developing `PSDSEL` expressions with syntax highlight and auto-correct features (similar to modern IDEs like
   IntelliJ).

## Legacy Syntax Problems

At People Spheres, we currently use two different expression syntaxes:

### First Legacy Syntax

```
input1.eachWordFirstChars(1) ~ '.' ~ (input2.wordsCount() > 1 ? input2.lastWords(-1).eachWordFirstChars(1) ~ input2.lastWords(1) : input2 ) ~ '@' ~ input3 ~ '.' ~ input4 ~ '.' ~ input5
```

### Second Legacy Syntax

```
getFirstLetter(input1).getWholeWord(input2).putConstant("@").getWholeWord.(input3).putConstant(".").getWholeWord.(input4)
```

Both of these syntaxes have the following downsides:

- Their linear nature is difficult for parsing by human operators
- They are very verbose and use characters that generate cognitive overhead
- They expose canonical email elements like `@` and the `.` used for concatenating the email’s domain part after `@`
- They are error-prone

## The new PSDSEL syntax

While designing the new `PSDSEL` we came up with three design propositions.

More details on this internal discussion and the three designs:
[here](https://gitlab.com/open-repositories/transformer/-/issues/4#additional-information).

After careful consideration, we choose this `PSDSEL` syntax:

![psdsel_syntax_example_1.png](readme-assets/psdsel_syntax_example_1.png)

We believe that this design has the following advantages:

1. Due to its non-linear, vertical structure, it is easy to read, understand and develop with;
2. The indentation further assists the expression author and the expression reader for both developing and understanding
   the expression;
3. Its JSON like structure is well-known for professionals with coding and technical background;
4. Its syntax is as close as possible to the syntax of any modern language and follows Java and Kotlin naming
   conventions (the primary technologies used by People Sphere’s backend team)
5. It does not force the developer and the reader to mentally evaluate email canonical elements like the `@` and the `.`
   that concatenates the email’s domain part.

# PSDSEL Syntax Rules

## Expression Syntax Structure

Each expression has three structural elements:

1. **Body:** starting with `generateEmail`and surrounded by `[]` brackets
2. **Components:**
    - *Name component*  starting with `withNameAs` and surrounded by `{}` brackets
    - *Domain component* starting with `atDomainAs` and surrounded by `{}` brackets
3. **Elements**: these are key-value pairs:
    - *Key*: the expression target
    - *Rule*: how the expression target must be transformed

## Expression Syntax Rules

### Order Matters

Ordering of the expression elements directly affects the final product because each component element is evaluated and
appended to the final product sequentially;

### Formatting is not relevant

Indentation, new lines and spaces in the provided `PSDSEL` example are not mandatory.

The ETS will happily interpret the same expression even when written as a single line with no white spaces.

### Arbitrary PSDSEL labels:

The following `PSDSEL` labels are not validated:

- `generateEmail`
- `withNameAs`
- `atDomainAs`

They represent our convention of describing the expression's purpose and the expression's components.

### Validation

Upon deserialization, we validate the expression syntax.

Your expression will **fail** validation if:

1. The body is not surrounded by `[]` brackets
2. The components are not surrounded by `{}` brackets
3. The components are not separated by `;`
4. The elements key-value pairs are not separated by `:`
5. The elements are not separated by `,`
6. The last element ends with `,`
7. You use unsupported or malformed targets and rule patterns.
8. You use unsupported name concatenation

### Allowed and supported expression targets

We currently support the following expression targets:

- `firstName`(John)
- `middleName` (Alexander)
- `lastName`  (Doe)
- `fullName` (John Alexander Doe)
- `role` (dev, hr, qa)
- `department` (backend, human resources, frontend)
- `resourceType` (internal, external)
- `domain` (com, fr, bg)
- `entity` (peoplespheres)
- `entityDomain`(peoplespheres.com)

<aside>
❗ Using malformed, mistyped or unsupported targets will result in `ErrorResponse` with detailed error message
</aside>


<aside>
❗ The query params must strictly follow the allowed expression targets. Introducing malformed, mistyped or unsupported query param will result in `ErrorResponse` with detailed error message
</aside>

### Allowed and supported expression rules

We currently support the following expression targets:

- `useAll` (Given ‘John’ will return ‘john’)
- `useFirst(2)` (Given ‘John’ will return ‘jo’)
- `useLast(1)`  (Given ‘John’ will return ‘n’)
- `concatenateWith`(allowed name concatenation characters are `_` , `-` , `.`)

<aside>
❗ Always surround the Integer argument with `()` brackets
</aside>


<aside>
❗ Using malformed, mistyped or unsupported expression rules will result in `ErrorResponse` with detailed error message
</aside>

# How Does PSDSEL Work?

An http request can contain many query parameters with the same key:

The ETS will:

1. Decode the expression;
2. Deserialize it to `EmailExpression` object;
3. Ignore all query parameters that are not part of the expression;
4. Use only the query parameters that match the expression targets defined in the expression elements;
5. Apply the expression rule for every expression target and transform the respective query parameter;
6. Filter out
   any [illegal characters](http://www.novell.com/documentation/groupwise2014r2/gwsdk_admin_rest_api/data/b12nem8w.html);
7. Validate that the final product is a valid email.

Given this url String:

```
localhost:9443/v1/transformer/generate/email?firstName=ivo&middleName=ivanov&lastName=georgiev&fullName=Ivo Ivanov Georgiev&role=dev&department=backend&resourceType=internal&entity=peoplespheres&domain=com&entityDomain=peoplespheres.com&expression=base-64-encoded-expression
```

and this expression

![psdsel_syntax_example_2.png](readme-assets/psdsel_syntax_example_2.png)

the ETS will generate the following output:

`ivo_i_iev@internal.peoplespheres.com`

# URL Rules

The URL must meet the following rules:

1. Query param keys must be valid expression targets:
    - `firstName`
    - `middleName`
    - `lastName`
    - `fullName`
    - `role`
    - `department`
    - `resourceType`
    - `domain`
    - `entity`
    - `entityDomain`
2. The `fullName` value can contain spaces (one or many). The ETS knows how to generate a valid email from a `fullName`
   expression target.
3. The expression parameter must be a `Base64` encoded `String`.
4. ETS can generate one or many emails if the URL contains param values for many users at a single company.

   Ideally - values related to a particular user should be grouped together:

    ```
    # Data for first user (Params order is not relevant)
    
    firstName:ivo
    middleName:ivanov
    lastName:georgiev
    fullName:Ivo Ivanov Ivanov
    role:dev
    department:backend
    resourceType:internal
    
    # Data for second user (Params order is not relevant)
    
    firstName:venelina
    middleName:ivanova
    lastName:ivanova
    fullName:Venelina Ivanova Ivanova
    role:pm
    department:pmo
    resourceType:external
    
    # Data for third user (Params order is not relevant)
    
    firstName:ventsislav
    middleName:nikolaev
    lastName:stoevski
    fullName:Ventsislav Nikolaev Stoevski
    role:dev
    department:backend
    resourceType:external
    
    # Values for generating the email domain part (Params order is not relevant)
    
    entity:peoplespheres
    domain:com
    entityDomain:peoplespheres.com
    
    # Expression
    
    expression:base-64-encoded-expression
    ```

   Given this URL (with the above query params):

    ```
    localhost:9443/v1/transformer/generate/email?firstName=ivo&middleName=ivanov&lastName=georgiev&fullName=Ivo Ivanov Georgiev&role=dev&department=backend&resourceType=internal&firstName=venelina&middleName=ivanova&lastName=ivanova&fullName=Venelina Ivanova Georgieva&role=pm&department=pmo&resourceType=external&firstName=ventsislav&middleName=nikolaev&lastName=stoevski&fullName=Ventsislav Nikolaev Stoevski&role=dev&department=backend&resourceType=external&entity=peoplespheres&domain=com&entityDomain=peoplespheres.com&expression=base-64-encoded-expression
    ```

   And this expression:

![psdsel_syntax_example_3.png](readme-assets/psdsel_syntax_example_3.png)

The ETS will return the following response:

```json
{
  "data": [
    {
      "id": "i_georgiev_dev@internal.peoplespheres.com",
      "value": "i_georgiev_dev@internal.peoplespheres.com"
    },
    {
      "id": "v_ivanova_pm@external.peoplespheres.com",
      "value": "v_ivanova_pm@external.peoplespheres.com"
    },
    {
      "id": "v_stoevski_dev@external.peoplespheres.com",
      "value": "v_stoevski_dev@external.peoplespheres.com"
    }
  ]
}
```

5. ETS can generate one or many emails for different companies if the URL contains param values for more than one user
   at a different company.

   Ideally - values related to a particular user should be grouped together:

    ```
    # Data for first user (Params order is not relevant)
    
    firstName:ivo
    middleName:ivanov
    lastName:georgiev
    fullName:Ivo Ivanov Georgiev
    role:dev
    department:backend
    resourceType:internal
    entity:peopleshperes
    domain:fr
    
    # Data for second user (Params order is not relevant
    
    firstName:venelina
    middleName:ivanova
    lastName:ivanova
    fullName:Venelina Ivanova Georgieva
    role:pm
    department:pmo
    resourceType:external
    entity:google
    domain:com
    
    # Data for third user (Params order is not relevant)
    
    firstName:ventsislav
    middleName:nikolaev
    lastName:stoevski
    fullName:Ventsislav Nikolaev Stoevski
    role:dev
    department:backend
    resourceType:external
    entity:cordona
    domain:tech
    
    # Expression
    
    expression:base-64-encoded-expression    
    ```

   Given this URL (with the above query params):

    ```
    localhost:9443/v1/transformer/generate/email?firstName=ivo&middleName=ivanov&lastName=georgiev&fullName=Ivo Ivanov Georgiev&role=dev&department=backend&resourceType=internal&entity=peopleshperes&domain=fr&firstName=venelina&middleName=ivanova&lastName=ivanova&fullName=Venelina Ivanova Georgieva&role=pm&department=pmo&resourceType=external&entity=google&domain=com&firstName=ventsislav&middleName=nikolaev&lastName=stoevski&fullName=Ventsislav Nikolaev Stoevski&role=dev&department=backend&resourceType=external&entity=cordona&domain=tech&expression=base-64-encoded-expression 
    ```

   And this expression:

   ![psdsel_syntax_example_3.png](readme-assets/psdsel_syntax_example_3.png)

   The ETS will return the following response:

```json
{
  "data": [
    {
      "id": "i.i.g.dev.backend@internal.peopleshperes.fr",
      "value": "i.i.g.dev.backend@internal.peopleshperes.fr"
    },
    {
      "id": "v.i.g.pm.pmo@external.google.com",
      "value": "v.i.g.pm.pmo@external.google.com"
    },
    {
      "id": "v.n.s.dev.backend@external.cordona.tech",
      "value": "v.n.s.dev.backend@external.cordona.tech"
    }
  ]
}
```

# Example Expressions

We included six examples of expressions, expected output and the Base64 encoded expression string.

All the provided examples were generated with actual calls to the ETS’s exposed endpoint with the following URL:

```
localhost:9443/v1/transformer/generate/email?firstName=ivo&middleName=ivanov&lastName=georgiev&fullName=Ivo Ivanov Georgiev&role=dev&department=backend&resourceType=internal&firstName=venelina&middleName=ivanova&lastName=ivanova&fullName=Venelina Ivanova Georgieva&role=pm&department=pmo&resourceType=external&firstName=ventsislav&middleName=nikolaev&lastName=stoevski&fullName=Ventsislav Nikolaev Stoevski&role=dev&department=backend&resourceType=external&entity=peoplespheres&domain=com&entityDomain=peoplespheres.com&expression=base-64-encoded-string
```

This particular call returns 3 emails, we included only the first one in
the [expression_samples.txt](readme-assets/expression_samples.txt)

You can download the file from the `readme-assets` folder.

# Postman collection

[On this link](https://speeding-water-256898.postman.co/workspace/PeopleSpheres-Transformer~d3e36d8b-0677-4cdc-aa69-ecbf63c7ac33/collection/10997458-9ee599b0-3edc-478a-8d52-aea5e4c8b684?action=share&creator=10997458)
you can find a collection of requests used in the above examples.

# Swagger

[On this link](http://localhost:8080/webjars/swagger-ui/index.html)
you can access the Swagger UI and try out the ETS API.

[On this link](http://localhost:8080/v3/api-docs)
you can get the documented API in JSON format.

# Planned for next iterations

We are still rapidly prototyping the `PSDSEL` and did not implement some features we consider essential.
They are planned for a future release after the approval of the senior stakeholders, the stakeholders from the
Expression Development Department and after our QA team confirm that our concept is valid.

1. `eval(someCondition)`

   Evaluating complex conditions like `someCondition ? doSomething : doSomethingElse` will consume significant resources
   and is postponed for next releases

2. `.of(n)`

   Currently - the `PSDSEL` can not operate on query parameter values on given position.

   Imagine a scenario where the query params contain `domain=fr`, `domain-com`, `domain=bg`

   Appending an `.of(n)` operator would allow the `PSDSEL` developer to write an expression that uses a value at a given
   index. For example, the expression target `domain.of(2)` will generate emails with .com (`PSDSEL` could be used by
   non-developers that start counting from `1` 😂 ).

3. Improved validation flow:

   Currently, the validation flow is concentrated in the email creation implementation.
   Single Responsibility-wise the validation will be nicely segregated, and the email creation logic will work only with
   already validated data

4. Configurable ETS:

   Currently, the business and validation rules are hardcoded as enum constants that are passable for this
   proof-of-concept stage.

   In the next iteration - the ETS will be refactored to be fully configurable.